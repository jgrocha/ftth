
-- DROP TABLE project.cable;

CREATE TABLE project.cable (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	uuid varchar NULL,
	material varchar(50) NULL DEFAULT 'COBRE'::character varying,
	"owner" varchar(50) NULL DEFAULT 'PT'::character varying,
	n_cable varchar(50) NULL DEFAULT '1'::character varying,
	obs varchar NULL,
	geom geometry(MULTILINESTRING, 3763) NULL,
	CONSTRAINT cable_pkey PRIMARY KEY (id)
);
CREATE INDEX cable_geom_idx ON project.cable USING gist (geom);

CREATE TABLE project.cable_drop (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	uuid varchar NULL,
	material varchar(50) NULL DEFAULT 'FO'::character varying,
	"owner" varchar(50) NULL DEFAULT 'DST'::character varying,
	n_cable varchar(50) NULL DEFAULT '1'::character varying,
	obs varchar NULL,
	geom geometry(MULTILINESTRING, 3763) NULL,
	CONSTRAINT cable_drop_pkey PRIMARY KEY (id)
);
CREATE INDEX cable_drop_geom_idx ON project.cable_drop USING gist (geom);

CREATE TABLE roe_area (
	id INT GENERATED ALWAYS AS identity primary key,
	uuid varchar NULL, -- 'ROE01'
	n_ua varchar NULL,
	cabinet_type varchar NULL,
	n_fo int NULL,
	obs varchar NULL,
	geom geometry(POLYGON, 3763) not NULL
);
CREATE INDEX roe_area_geom_idx ON roe_area USING gist (geom);

CREATE TABLE pld (
	id INT GENERATED ALWAYS AS identity primary key,
	uuid varchar NULL, -- 'PLDF/33'
	obs varchar NULL,
	geom geometry(POINT, 3763) not NULL
);
CREATE INDEX pld_geom_idx ON pld USING gist (geom);

CREATE TABLE pld_area (
	id INT GENERATED ALWAYS AS identity primary key,
	uuid varchar NULL, -- 'PLDF/33'
	n_ua int NULL,
	obs varchar NULL,
	geom geometry(POLYGON, 3763) not NULL
);
CREATE INDEX pld_area_geom_idx ON pld_area USING gist (geom);

ALTER TABLE project.pld_area ALTER COLUMN n_ua TYPE int USING n_ua::int;

drop table ua;

CREATE TABLE ua (
	id INT GENERATED ALWAYS AS identity primary key,
	uuid varchar NULL, --  '000715'
	pld varchar NULL, --  'PLDF/33'
	pld_type varchar null, -- 'P', P|F|C
	n_ua_residential int NOT null default 0,
	n_ua_comercial int NOT null default 0,
	n_ua_companies int NOT null default 0,
	n_floors int NOT null default 0,
	obs varchar NULL,
	geom geometry(POINT, 3763) not NULL
);
CREATE INDEX ua_geom_idx ON ua USING gist (geom);

select * frpm pld where id = 418;

select * from pld where uuid is not null;
select * from pld_area where uuid is not null;

update pld set uuid = 'Vénus de Barcelos' where id = 424;

update pld_area
set uuid = pld.uuid 
from pld 
where st_within(pld.geom, pld_area.geom)
and pld.id = 418;

select count(pld_area.*)
from pld_area, pld 
where st_within(pld.geom, pld_area.geom)
and pld.id = 418;

--PL/pgSQL (Chapter 40), 
--PL/Tcl (Chapter 41), 
--PL/Perl (Chapter 42), 
--PL/Python (Chapter 43)
-- https://www.postgresql.org/docs/current/plpython-database.html

DROP FUNCTION update_pld_area();
DROP function project.update_pld_area() cascade;

select * from pld where uuid is not null;
select * from pld_area where uuid is not null;
update pld set uuid = 'Ritinha' where id = 424;

CREATE OR REPLACE FUNCTION project.py_update_pld_area() RETURNS TRIGGER AS $$
uuid = TD['new']['uuid']
geom = TD['new']['geom']
u1 = plpy.execute("update project.pld_area set uuid = '{}' where st_within('{}'::geometry, geom)".format(uuid, geom))
u2 = plpy.execute("update project.ua u set uuid = '{}' from project.pld_area a where st_within(u.geom, a.geom) and st_within('{}'::geometry, a.geom)".format(uuid, geom))
$$ LANGUAGE plpython3u;


CREATE OR REPLACE FUNCTION project.py_update_pld_area() RETURNS TRIGGER AS $$
uuid = TD['new']['uuid']
geom = TD['new']['geom']
plpy.notice('Viva! {}'.format(uuid))
r = plpy.execute("select count(*) from project.pld_area where st_within('{}'::geometry, pld_area.geom)".format(geom))
# plpy.notice(r)
if r.nrows() > 0:
	plpy.notice(r[0]['count'])
	u = plpy.execute("update project.pld_area set uuid = '{}' where st_within('{}'::geometry, geom)".format(uuid, geom))
	plpy.notice(u)
$$ LANGUAGE plpython3u;

do $$
plpy.notice('teste')
$$ language plpython3u;

DROP TRIGGER update_pld_area_trigger ON project.pld;

CREATE TRIGGER update_pld_area_trigger BEFORE insert or UPDATE ON project.pld
    FOR EACH ROW
    EXECUTE PROCEDURE project.py_update_pld_area();

-- sudo apt install postgresql-plpython3-12 postgresql-plpython3-13
-- sudo apt install postgresql-plpython3-12 postgresql-plpython3-13

create extension plpython3u;

CREATE FUNCTION pymax (a integer, b integer)
  RETURNS integer
AS $$
  if a > b:
    return a
  return b
$$ LANGUAGE plpython3u;


---------------------------------------------------------------------
select * from almaceda3763.cabos_telecom ct ;

insert into project.cable( uuid, material, "owner", n_cable, geom)
select id, material, proprietario, 
CASE WHEN numero_cabos ~E'^\\d+$' THEN numero_cabos::integer ELSE 0 END, shape
from almaceda3763.cabos_telecom;

truncate project.cable;

-- juntar as duas redes de cabos
insert into project.cable( uuid, material, "owner", n_cable, geom)
select id, material, proprietario, 
CASE WHEN numero_cabos ~E'^\\d+$' THEN numero_cabos::integer ELSE 0 END, shape
from almaceda3763.cabos_edp;

-- criar grafo
alter table almaceda3763.cabos_telecom add column source int null;
alter table almaceda3763.cabos_telecom add column target int null;

select pgr_createTopology('almaceda3763.cabos_telecom', 0.1, the_geom := 'shape', id := 'objectid' );

alter table project.cable add column source bigint null;
alter table project.cable add column target bigint null;


-- verificar que não há linhas duplicadas
select a.*
from project.cable a
join project.cable b on st_coveredby(b.geom,a.geom)
where a.id != b.id;
-- remover linhas duplicadas
with repetidos as (
  select a.id + b.id as par, a.*
  from project.cable a
  join project.cable b on st_coveredby(b.geom,a.geom)
  where a.id != b.id),
duplos as (select max(id) from repetidos
 group by par)
 delete from project.cable where id in (select * from duplos);
-- partir a rede nos pontos de interseção: MultiLineString → LineString
create table cables_2 as 
  Select 
    row_number() over() as id,
    a.uuid,
    a.material,
    a."owner",
    a.n_cable,
    a.obs,
    (st_dump(st_split(a.geom, b.geom))).geom as geom
  from project.cable a
  join project.cable b on st_intersects(a.geom, b.geom)
  and a.id != b.id;
-- eliminar as linhas maiores que se sobrepõem às menores que resultaram do split
delete from project.cables_2 where id in (
  select distinct a.id
  from project.cables_2 a
  join project.cables_2 b on st_coveredby(b.geom,a.geom)
  where a.id != b.id and st_length(a.geom) > st_length(b.geom)
  );
-- criar topologia
truncate project.cable;
truncate project.cable;
insert into project.cable( uuid, material, "owner", n_cable, geom)
select uuid, material, "owner", n_cable, st_Multi(geom)
from project.cables_2;
-- alter table project.cable add column source bigint null;
-- alter table project.cable add column target bigint null;
-- este é o que está em uso
select pgr_createTopology('project.cable', 0.1, the_geom := 'geom', clean:=true);

/*
Performing checks, please wait .....
Creating Topology, Please wait...
-------------> TOPOLOGY CREATED FOR  955 edges
Rows with NULL geometry or NULL id: 0
Vertices table for table almaceda3763.cabos_telecom is: almaceda3763.cabos_telecom_vertices
*/

select pgr_dijkstra('select * from almaceda3763.cabos_telecom', 1, 2);

SELECT * from pgr_dijkstra( 'select id, source, target, st_length(geom) as cost from project.cable', 288, 294, directed:=false);
 
SELECT * from pgr_dijkstra( 'select id, source, target, st_length(geom) as cost from project.cable', 2153, 2162, directed:=false);

-- nodo mais perto de uma data PLD
SELECT * FROM cable_vertices_pgr cvp 
    ORDER BY the_geom <-> ST_GeometryFromText('POINT(40410.92946909547754331 37716.43562635757552925)',3763) 
    LIMIT 1;

-- node mais perto de uma dado UA
SELECT * FROM cable_vertices_pgr cvp 
    ORDER BY the_geom <-> ST_GeometryFromText('POINT(40420.46635763918311568 37717.32257280840713065)',3763) 
    LIMIT 1; 

-- plano no curto prazo:
-- Dados dois pontos selecionados, calcular o caminho (para se poder afinar os pesos
-- Dado um PLD, desenhar a rede DROP a partir desse PLD
--   Ligar o PLD a cada uma das UA na respetiva área 

-- primeiro segmento

   -- calcular o POSTE mais próximo do PLD
   -- retorna o ID do POSTE (vértice do grafo) e a geometria alinha que liga o PLD ao POSTE
with origem as (select * from pld where id = 408),
area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)),
proximo as (
	SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom 
	FROM cable_vertices_pgr cvp, origem, area 
	where st_contains(area.geom, cvp.the_geom)
	ORDER BY 2 limit  1)
select proximo.id, proximo.distancia, origem.uuid, 'Generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) as geom from proximo, origem;

   -- calcular o POSTE mais próximo da UA
   -- retorna o ID do POSTE (vértice do grafo) e a geometria alinha que liga a UA ao POSTE
with origem as (select * from project.ua where id = 219),
area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)),
proximo as (
	SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom 
	FROM cable_vertices_pgr cvp, origem, area 
	where st_contains(area.geom, cvp.the_geom)
	ORDER BY 2 limit  1)
select proximo.id, proximo.distancia, origem.uuid, 'Generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) as geom from proximo, origem;

   -- calcular o PLD mais próximo da UA
   -- retorna o ID do PLD e a geometria alinha que liga a UA ao PLD
   -- só há um PLD
with origem as (select * from project.ua where id = 219),
area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)),
pld_ua as (select pld.* from area a, pld where st_contains(a.geom, pld.geom))
select pld_ua.id, pld_ua.geom <-> origem.geom as distancia, origem.uuid, st_multi(ST_MakeLine(pld_ua.geom, origem.geom)) as geom 
from pld_ua, origem;

select ua2pld(219);
select ua2pole(219);

with origem as (select * from pld where id = 408),
area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)),
proximo as (
	SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom 
	FROM cable_vertices_pgr cvp, origem, area 
	where st_contains(area.geom, cvp.the_geom)
	ORDER BY 2 limit  1)
select proximo.id, proximo.distancia, origem.uuid, 'Generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) as geom from proximo, origem;

select pld2pole(408);

select * from cable_vertices_pgr cvp;

create or replace FUNCTION pld2pole (id_pld integer) returns SETOF drop_type
AS $$
pole_str = (
	"with origem as (select * from project.pld where id = {}), "
	"area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)), "
	"proximo as ( "
	"	SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom "
	"	FROM cable_vertices_pgr cvp, origem, area "
	"	where st_contains(area.geom, cvp.the_geom) "
	"  ORDER BY 2 limit  1) "
	"select proximo.id, proximo.distancia, origem.uuid, 'Generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) as geom "
	"from proximo, origem"
)
r = plpy.execute(pole_str.format(id_pld))
# plpy.notice(r)
return r;
$$ LANGUAGE plpython3u;

create or replace FUNCTION ua2pole (id_ua integer) returns SETOF drop_type
AS $$
pole_str = (
	"with origem as (select * from project.ua where id = {}), "
	"area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)), "
	"proximo as ( "
	"	SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom "
	"	FROM cable_vertices_pgr cvp, origem, area "
	"	where st_contains(area.geom, cvp.the_geom) "
	"  ORDER BY 2 limit  1) "
	"select proximo.id, proximo.distancia, origem.uuid, 'Generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) as geom "
	"from proximo, origem"
)
r = plpy.execute(pole_str.format(id_ua))
# plpy.notice(r)
return r;
$$ LANGUAGE plpython3u;

create or replace FUNCTION ua2pld (id_ua integer) returns SETOF drop_type
AS $$
pld_str = (
	"with origem as (select * from project.ua where id = {}), "
	"area as (select a.* from pld_area a, origem p where st_contains(a.geom, p.geom)), "
	"pld_ua as (select pld.* from area a, pld where st_contains(a.geom, pld.geom)) "
	"select pld_ua.id, pld_ua.geom <-> origem.geom as distancia, origem.uuid, st_multi(ST_MakeLine(pld_ua.geom, origem.geom)) as geom  "
	"from pld_ua, origem"
)
r = plpy.execute(pld_str.format(id_ua))
# plpy.notice(r)
return r;
$$ LANGUAGE plpython3u;

CREATE TYPE drop_type AS (id int, distancia float8, uuid text, geom geometry);



drop PROCEDURE drop4ua2pld (id_pld integer, id_ua integer);



CREATE or replace PROCEDURE drop4ua2pld (id_ua integer)
AS $$
drop1 = plpy.execute("select ua2pld({})".format(id_ua))
drop2 = plpy.execute("select ua2pole({})".format(id_ua))
plpy.notice(drop1[0]['ua2pld']['distancia'])
distance_to_pld = drop1[0]['ua2pld']['distancia']
distance_to_pole = drop2[0]['ua2pole']['distancia']
if (distance_to_pld < distance_to_pole * 1.5):
	plpy.notice('Connect directly to PLD')
	#-- insert just one segment (drop1)
	result_str = (
		"insert into project.cable_drop (uuid, obs, geom) "
		"select {}, 'Generated', '{}'::geometry"
	)
	plpy.notice(result_str.format( drop1[0]['ua2pld']['uuid'], drop1[0]['ua2pld']['geom'] ))
	res = plpy.execute(result_str.format( drop1[0]['ua2pld']['uuid'], drop1[0]['ua2pld']['geom'] ) )
	plpy.notice(res)
else:
	plpy.notice('Connect to Pole')
	#-- PLD must also be connected to its nearst pole
	#-- 2x segments needed + segments between poles
	pld_id = drop1[0]['ua2pld']['id']
	drop3 = plpy.execute("select pld2pole({})".format(pld_id))
	plpy.notice(drop3)
	#-- route between 
	pole_start = drop2[0]['ua2pole']['id']
	pole_end = drop3[0]['pld2pole']['id']
	plpy.notice(pole_start, ' → ', pole_end)
	result_str = (
		"insert into project.cable_drop (uuid, obs, geom) "
		"with pole2pole as ( "
		"select st_union(geom) as geom from project.cable "
		"where id in ( "
		"	SELECT edge from pgr_dijkstra( 'select id, source, target, st_length(geom) as cost from project.cable', {}, {}, directed:=false) "
		"where edge != -1"
		")) "
		"select {}, 'Generated', st_union(st_union(pole2pole.geom, '{}'::geometry), '{}'::geometry) from pole2pole "
	)
	plpy.notice(drop3[0]['pld2pole']['geom'])
	plpy.notice(drop2[0]['ua2pole']['geom'])
	res = plpy.execute(result_str.format( pole_start, pole_end, drop1[0]['ua2pld']['uuid'], drop3[0]['pld2pole']['geom'], drop2[0]['ua2pole']['geom']) )
	plpy.notice(res)
$$ LANGUAGE plpython3u;

delete from cable_drop;
call drop4ua2pld(327);
call drop4ua2pld(308);
call drop4ua2pld(294);

call drop4ua2pld(234);
call drop4ua2pld(220);

insert into project.cable_drop (uuid, obs, geom) 
select 000580, 'Generated', '0105000020B30E000001000000010200000002000000F7F835BE5DBBE340E2AFA6F08D6AE2405F384B09E5BBE3409E7D2CA27669E240'::geometry;


insert into project.cable_drop (uuid, obs, geom)
with pole2pole as (
select st_union(geom) as geom from project.cable
where id in (
	SELECT edge from pgr_dijkstra( 'select id, source, target, st_length(geom) as cost from project.cable', 1211, 1220, directed:=false)
	where edge != -1
))
select '007', 'Generated', st_union(st_union(pole2pole.geom, '0105000020B30E000001000000010200000002000000E457FB6423BBE3404151A6C5A16AE240F7F835BE5DBBE340E2AFA6F08D6AE240'::geometry), '0105000020B30E000001000000010200000002000000871F0B4265BCE34097739EB78A68E24086DD8D0294BCE3402BE4511CEB67E240'::geometry) 
from pole2pole;



-- DROP da UA para o poste ou diretamente para o PLD
-- Tenho que ter um IF
-- Se for diretamente par ao PLD, já não preciso de calcular o melhor caminho
insert into project.cable_drop (uuid, obs, geom)
with origem as (select * from project.ua where id = 219),
poste_proximo as (SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom as the_geom, 'Poste' as target FROM cable_vertices_pgr cvp, origem ORDER BY 2 asc limit  1),
pld_proximo as (SELECT pld.id, pld.geom <-> origem.geom as distancia,  pld.geom as the_geom, 'PLD' as target from pld, origem where pld.id = 408 ORDER BY 2 asc limit  1),
resultado as (select * from poste_proximo
union
select * from pld_proximo
order by distancia limit 1)
select origem.uuid, 'Generated', st_multi(ST_MakeLine(resultado.the_geom, origem.geom)) from resultado, origem;

call pypld2pole( 407 );

CREATE PROCEDURE pypld2pole (id_pld integer)
AS $$
insert_str = (
	"insert into project.cable_drop (uuid, obs, geom) "
	"with origem as (select * from project.pld where id = {}), "
	"proximo as (SELECT cvp.id,  cvp.the_geom <-> origem.geom as distancia, cvp.the_geom FROM project.cable_vertices_pgr cvp, origem ORDER BY 2 asc limit  1) "
	"select origem.uuid, 'generated', st_multi(ST_MakeLine(proximo.the_geom, origem.geom)) from proximo, origem"
)
r = plpy.execute(insert_str.format(id_pld))
plpy.notice(r)
$$ LANGUAGE plpython3u;


CREATE OR REPLACE FUNCTION project.py_update_pld_area() RETURNS TRIGGER AS $$
uuid = TD['new']['uuid']
geom = TD['new']['geom']
plpy.notice('Viva! {}'.format(uuid))
r = plpy.execute("select count(*) from project.pld_area where st_within('{}'::geometry, pld_area.geom)".format(geom))
# plpy.notice(r)
if r.nrows() > 0:
	plpy.notice(r[0]['count'])
	u = plpy.execute("update project.pld_area set uuid = '{}' where st_within('{}'::geometry, geom)".format(uuid, geom))
	plpy.notice(u)
$$ LANGUAGE plpython3u;
