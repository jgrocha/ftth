# Import DWG

## Base de dados

* Escolher a ligação à base de dados
* Escolher o nome para o esquema

## Convert o DWG para geopackage

Na linha de comandos ou através do QGIS (pode ser preciso ter o GDAL compilado com o ODA)

```
ogr2ogr -f "GPKG" cli.gpkg -s_srs EPSG:27493 -t_srs EPSG:3763 C11_CTB_MI_R01.dwg entities
``` 

Bug no QGIS. Depois de exportar, no Browser o QGIS queixa-se de layer inválido.

```
ogr2ogr -f "GPKG" cli.gpkg -s_srs EPSG:27493 -t_srs EPSG:3763 SS_Borba.dwg entities
ogr2ogr -f PostgreSQL "PG:service=ftth" SS_Borba.dwg -s_srs EPSG:27493 -t_srs EPSG:3763 -nln borba.entities
``` 

## Import ROE Area

Filter sobre a camada entities

Filter: ("Layer" ILIKE '%00-Opcao1(2ROE)_Limites ROE%')

Select row

Vector → Geometry Tools → Lines to polygons

Input: layer entities
Selected features only

Criação de um layer temporario

Editar o layer temporario e fazer copy and paste do único feature para o layer roe_area
Edit layer temporario → Copy features
Edit roe_area → Paste features


## Import PLD Area

Filter: Layer = '09-Acesso-PLD'

Convert linhas para polígonos

## Import PLD

Filter: Text ilike 'PLD %'

## Import UA

Filter: ("SubClasses" ILIKE '%AcDbEntity:AcDbText:AcDbAttribute%') AND (("Text" ILIKE 'PLDP%') OR ("Text" ILIKE 'PLDF%'))

